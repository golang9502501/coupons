package validator

import (
	"coupons/coupons/errs"
	"errors"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	enTranslations "github.com/go-playground/validator/v10/translations/en"
	"strings"
)

type Validator struct {
	validate  *validator.Validate
	translate ut.Translator
}

func NewValidator() (*Validator, error) {
	translator := en.New()
	uni := ut.New(translator, translator)

	transEN, found := uni.GetTranslator(translator.Locale())

	if !found {
		return nil, errors.New("translator not found")
	}

	v := Validator{validate: validator.New(), translate: transEN}
	if err := v.registerTranslations(); err != nil {
		return nil, err
	}

	return &v, nil
}

func (v *Validator) ValidateStruct(s interface{}) error {
	err := v.validate.Struct(s)

	if err == nil {
		return nil
	}

	var valErrs validator.ValidationErrors
	var errMessages []string

	if errors.As(err, &valErrs) {
		for _, e := range valErrs {
			errMessages = append(errMessages, e.Translate(v.translate))
		}
	}

	return errs.NewErrorInvalidData(strings.Join(errMessages, ", "))
}

func (v *Validator) registerTranslations() error {
	if err := enTranslations.RegisterDefaultTranslations(v.validate, v.translate); err != nil {
		return err
	}

	if err := v.validate.RegisterTranslation("required", v.translate, func(ut ut.Translator) error {
		return ut.Add("required", "{0} is a required field", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("required", fe.Field())
		return t
	}); err != nil {
		return err
	}

	if err := v.validate.RegisterTranslation("max", v.translate, func(ut ut.Translator) error {
		return ut.Add("max", "{0} is too long", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("max", fe.Field())
		return t
	}); err != nil {
		return err
	}

	return nil
}
