package logger

import (
	"github.com/rs/zerolog"
	"os"
	"time"
)

func NewLogger(level zerolog.Level) *zerolog.Logger {
	writer := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	logger := zerolog.New(writer).With().Timestamp().Logger().Level(level)

	return &logger
}
