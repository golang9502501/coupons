package helper

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"google.golang.org/grpc/status"
)

type AnyUuid struct{}

func (a AnyUuid) Match(v driver.Value) bool {
	uuidStr, ok := v.(string)
	if !ok {
		return false
	}
	_, err := uuid.Parse(uuidStr)

	return err == nil
}

func TestCompErrs(real interface{}, expect interface{}) error {
	if real == nil && expect == nil {
		return nil
	}

	er, ok := real.(error)
	if !ok {
		return errors.New("Err -> \n real is not error")
	}

	erExp, ok := expect.(error)
	if !ok {
		return errors.New("Err -> \n expectation is not error")
	}

	if er.Error() != erExp.Error() {
		return errors.New(fmt.Sprintf("Err -> \nWant: %q\nGot: %q\n", erExp, er))
	}

	type grpcError interface {
		GRPCStatus() *status.Status
	}

	erGrpc, ok := er.(grpcError)
	erExpGrpc, okExp := erExp.(grpcError)

	if ok != okExp {
		return errors.New(fmt.Sprintf("Err (incompatible error types) -> \nWant: grpc %t\nGot grpc: %t\n", okExp, ok))
	}

	if !ok && !okExp {
		return nil
	}

	if erGrpc.GRPCStatus().Code().String() != erExpGrpc.GRPCStatus().Code().String() {
		return errors.New(fmt.Sprintf("Err (incompatible grpc status) -> \nWant: %s\nGot: %s\n", erExpGrpc.GRPCStatus().Code().String(), erGrpc.GRPCStatus().Code().String()))
	}

	return nil
}
