package helper

import (
	"coupons/coupons/errs"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHelper_TestCompErrs(t *testing.T) {
	assertions := assert.New(t)

	assertions.Equal(
		nil,
		TestCompErrs(nil, nil),
	)
	assertions.Equal(
		nil,
		TestCompErrs(errors.New("some error"), errors.New("some error")),
	)
	assertions.Equal(
		"Err -> \n real is not error",
		TestCompErrs("invalid type", errors.New("some error")).Error(),
	)
	assertions.Equal(
		"Err -> \n expectation is not error",
		TestCompErrs(errors.New("some error"), "invalid type").Error(),
	)
	assertions.Equal(
		"Err -> \nWant: \"some another error\"\nGot: \"some error\"\n",
		TestCompErrs(errors.New("some error"), errors.New("some another error")).Error(),
	)
	assertions.Equal(
		"Err (incompatible error types) -> \nWant: grpc true\nGot grpc: false\n",
		TestCompErrs(errors.New("some error"), errs.NewErrorBadQuery("some error")).Error(),
	)
	assertions.Equal(
		"Err (incompatible grpc status) -> \nWant: AlreadyExists\nGot: Internal\n",
		TestCompErrs(errs.NewErrorBadQuery("some error"), errs.NewErrorAlreadyExists("some error")).Error(),
	)
}
