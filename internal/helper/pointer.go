package helper

func PStr(val string) *string {
	return &val
}

func PUint32(val uint32) *uint32 {
	return &val
}

func PUint64(val uint64) *uint64 {
	return &val
}
