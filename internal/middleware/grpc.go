package middleware

import (
	"context"
	"google.golang.org/grpc"
	"time"
)

func ContextTimeoutUnaryInterceptor(timeout time.Duration) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		ctx2, cancel := context.WithTimeout(ctx, timeout)
		defer cancel()

		return handler(ctx2, req)
	}
}
