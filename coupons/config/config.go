package config

import (
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"time"
)

type Config struct {
	Grpc     Grpc
	Database Database
}

type Grpc struct {
	Port    int           `envconfig:"GRPC_PORT"`
	Timeout time.Duration `envconfig:"GRPC_TIMEOUT"`
}

type Database struct {
	Port     int    `envconfig:"DB_PORT"`
	Host     string `envconfig:"DB_HOST"`
	Database string `envconfig:"DB_DATABASE"`
	Username string `envconfig:"DB_USERNAME"`
	Password string `envconfig:"DB_PASSWORD"`
}

func NewConfig() (*Config, error) {
	if err := godotenv.Load(".env"); err != nil {
		return nil, err
	}

	var cfg Config
	if err := envconfig.Process("", &cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}
