package dto

import (
	"coupons/coupons/errs"
	"coupons/internal/validator"
	"github.com/google/uuid"
)

type CreateCoupon struct {
	Title       string `validate:"required,max=255"`
	Description string `validate:"max=65535"`
	Promocode   string `validate:"required,max=255"`
}

func (e CreateCoupon) Validate(v *validator.Validator) error {
	return v.ValidateStruct(e)
}

type EditCoupon struct {
	Uuid        uuid.UUID
	Title       *string
	Description *string
	Promocode   *string
}

func (e EditCoupon) Validate(v *validator.Validator) error {

	if e.Title == nil && e.Description == nil && e.Promocode == nil {
		return errs.NewErrorInvalidData("at least one updated field must be filled in")
	}

	var (
		title       string
		description string
		promocode   string
	)

	if e.Title != nil {
		title = *e.Title
	}
	if e.Description != nil {
		description = *e.Description
	}
	if e.Promocode != nil {
		promocode = *e.Promocode
	}

	return v.ValidateStruct(struct {
		Title       string `validate:"max=255"`
		Description string `validate:"max=65535"`
		Promocode   string `validate:"max=255"`
	}{
		Title:       title,
		Description: description,
		Promocode:   promocode,
	})
}

type GetCoupons struct {
	Page  uint64
	Limit uint32
	Title string `validate:"max=255"`
}

func (e GetCoupons) Validate(v *validator.Validator) error {
	return v.ValidateStruct(e)
}
