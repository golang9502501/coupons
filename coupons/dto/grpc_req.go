package dto

import (
	"coupons/coupons/coupons_gen"
	"coupons/coupons/errs"
	"github.com/google/uuid"
)

type CreateCouponRequest struct {
	*coupons_gen.CreateCouponRequest
}

func NewCreateCouponRequest(req *coupons_gen.CreateCouponRequest) CreateCouponRequest {
	return CreateCouponRequest{CreateCouponRequest: req}
}

func (req CreateCouponRequest) ToEntity() (CreateCoupon, error) {
	return CreateCoupon{
		Title:       req.GetTitle(),
		Description: req.GetDescription(),
		Promocode:   req.GetPromocode(),
	}, nil
}

type GetCouponRequest struct {
	*coupons_gen.GetCouponRequest
}

func NewGetCouponRequest(req *coupons_gen.GetCouponRequest) GetCouponRequest {
	return GetCouponRequest{GetCouponRequest: req}
}

func (req GetCouponRequest) ToUuid() (uuid.UUID, error) {
	uuidVal, err := uuid.Parse(req.Id)
	if err != nil {
		return uuid.UUID{}, errs.NewErrorInvalidData("invalid uuid: " + err.Error())
	}

	return uuidVal, nil
}

type GetCouponsRequest struct {
	*coupons_gen.GetCouponsRequest
}

func NewGetCouponsRequest(req *coupons_gen.GetCouponsRequest) GetCouponsRequest {
	return GetCouponsRequest{GetCouponsRequest: req}
}

func (req GetCouponsRequest) ToEntity(defLimit uint32) (GetCoupons, error) {
	page := req.GetPage()
	if page == 0 {
		page = 1
	}

	limit := req.GetLimit()
	if limit == 0 {
		limit = defLimit
	}

	return GetCoupons{
		Page:  page,
		Limit: limit,
		Title: req.GetTitle(),
	}, nil
}

type EditCouponRequest struct {
	*coupons_gen.EditCouponRequest
}

func NewEditCouponRequest(req *coupons_gen.EditCouponRequest) EditCouponRequest {
	return EditCouponRequest{EditCouponRequest: req}
}

func (req EditCouponRequest) ToEntity() (EditCoupon, error) {
	uuidVal, err := uuid.Parse(req.GetId())
	if err != nil {
		return EditCoupon{}, errs.NewErrorInvalidData("invalid uuid: " + err.Error())
	}

	return EditCoupon{
		Uuid:        uuidVal,
		Title:       req.Title,
		Description: req.Description,
		Promocode:   req.Promocode,
	}, nil
}

type DeleteCouponRequest struct {
	*coupons_gen.DeleteCouponRequest
}

func NewDeleteCouponRequest(req *coupons_gen.DeleteCouponRequest) DeleteCouponRequest {
	return DeleteCouponRequest{DeleteCouponRequest: req}
}

func (req DeleteCouponRequest) ToUuid() (uuid.UUID, error) {
	uuidVal, err := uuid.Parse(req.Id)
	if err != nil {
		return uuid.UUID{}, errs.NewErrorInvalidData("invalid uuid: " + err.Error())
	}

	return uuidVal, nil
}
