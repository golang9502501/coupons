package dto

import (
	"coupons/coupons/coupons_gen"
	"github.com/google/uuid"
)

type Coupon struct {
	Uuid        uuid.UUID
	Title       string
	Description string
	Promocode   string
}

func (e Coupon) ToGrpc() (*coupons_gen.CouponResponse, error) {
	return &coupons_gen.CouponResponse{
		Id:          e.Uuid.String(),
		Title:       e.Title,
		Description: e.Description,
		Promocode:   e.Promocode,
	}, nil
}

type CouponListPagination struct {
	Page  uint64
	Limit uint32
	Pages uint64
	Total uint64
	List  []Coupon
}

func (e CouponListPagination) ToGrpc() (*coupons_gen.CouponListPaginationResponse, error) {
	list := make([]*coupons_gen.CouponResponse, len(e.List))

	for k, v := range e.List {
		grpc, err := v.ToGrpc()
		if err != nil {
			return nil, err
		}

		list[k] = grpc
	}

	return &coupons_gen.CouponListPaginationResponse{
		Page:    e.Page,
		Limit:   e.Limit,
		Total:   e.Total,
		Pages:   e.Pages,
		Coupons: list,
	}, nil
}
