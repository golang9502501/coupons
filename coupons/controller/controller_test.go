package controller

import (
	"context"
	"coupons/coupons/coupons_gen"
	"coupons/coupons/errs"
	"coupons/coupons/repository"
	"coupons/coupons/usecase"
	"coupons/internal/helper"
	"coupons/internal/logger"
	"coupons/internal/validator"
	"database/sql"
	"database/sql/driver"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/resolver"
	"google.golang.org/grpc/test/bufconn"
	"log"
	"net"
	"testing"
)

func server(db *sql.DB) coupons_gen.GrpcCouponsServiceClient {
	validate, err := validator.NewValidator()
	if err != nil {
		log.Fatalf("test validator init: %e", err)
	}

	s := grpc.NewServer()

	repo := repository.NewCouponMariaDb(db, logger.NewLogger(zerolog.Disabled))
	uc := usecase.NewCouponUseCase(repo)
	c := NewCouponController(uc, validate)

	coupons_gen.RegisterGrpcCouponsServiceServer(s, &c)

	listener := bufconn.Listen(1024 * 1024)

	go func() {
		if err := s.Serve(listener); err != nil {
			log.Fatalf("test grpc server run: %e", err)
		}
	}()

	resolver.SetDefaultScheme("passthrough")

	conn, err := grpc.NewClient("bufnet", grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}), grpc.WithInsecure())
	if err != nil {
		log.Fatalf("test dial: %e", err)
	}

	return coupons_gen.NewGrpcCouponsServiceClient(conn)
}

func TestCouponsService_CreateCoupon(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	client := server(db)
	ctx := context.Background()

	type expectation struct {
		response *coupons_gen.CouponResponse
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  *coupons_gen.CreateCouponRequest
		expected expectation
	}{
		"success": {
			request: &coupons_gen.CreateCouponRequest{
				Title:       "-25% на первый и повторные онлайн-заказы",
				Description: helper.PStr("Небольшое описание"),
				Promocode:   "RADUGA1234",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				sql: func() {
					uuidStr := uuid.New().String()
					mock.
						ExpectQuery("SELECT uuid, title, description, promocode FROM coupons WHERE promocode = ?").
						WithArgs("RADUGA1234").
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}))
					mock.
						ExpectQuery(`INSERT INTO coupons \(uuid,title,description,promocode\) VALUES \(\?,\?,\?,\?\) RETURNING uuid`).
						WithArgs(helper.AnyUuid{}, "-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234").
						WillReturnRows(sqlmock.NewRows([]string{"uuid"}).AddRow(uuidStr))
					mock.
						ExpectQuery("SELECT uuid, title, description, promocode FROM coupons WHERE uuid = ?").
						WithArgs(uuidStr).
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}).AddRow(uuidStr, "-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234"))

				},
			},
		},
		"success_without_desc": {
			request: &coupons_gen.CreateCouponRequest{
				Title:       "-30% на крем от загара",
				Description: nil,
				Promocode:   "LETO2024",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{
					Title:       "-30% на крем от загара",
					Description: "",
					Promocode:   "LETO2024",
				},
				err: nil,
				sql: func() {
					uuidStr := uuid.New().String()
					mock.
						ExpectQuery("SELECT uuid, title, description, promocode FROM coupons WHERE promocode = ?").
						WithArgs("LETO2024").
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}))
					mock.
						ExpectQuery(`INSERT INTO coupons \(uuid,title,description,promocode\) VALUES \(\?,\?,\?,\?\) RETURNING uuid`).
						WithArgs(helper.AnyUuid{}, "-30% на крем от загара", "", "LETO2024").
						WillReturnRows(sqlmock.NewRows([]string{"uuid"}).AddRow(uuidStr))
					mock.
						ExpectQuery("SELECT uuid, title, description, promocode FROM coupons WHERE uuid = ?").
						WithArgs(uuidStr).
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}).AddRow(uuidStr, "-30% на крем от загара", "", "LETO2024"))

				},
			},
		},
		"failed_without_title": {
			request: &coupons_gen.CreateCouponRequest{
				Title:       "",
				Description: nil,
				Promocode:   "LETO2024",
			},
			expected: expectation{
				response: nil,
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = Title is a required field"),
				sql:      func() {},
			},
		},
		"failed_duplicate_promocode": {
			request: &coupons_gen.CreateCouponRequest{
				Title:       "-30% на крем от загара",
				Description: nil,
				Promocode:   "LETO2024",
			},
			expected: expectation{
				response: nil,
				err:      errs.NewErrorAlreadyExists("rpc error: code = AlreadyExists desc = coupon with this promocode already exists"),
				sql: func() {
					uuidStr := uuid.New().String()
					mock.
						ExpectQuery("SELECT uuid, title, description, promocode FROM coupons WHERE promocode = ?").
						WithArgs("LETO2024").
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}).AddRow(uuidStr, "Тест-заголовок", "Тест-описание", "LETO2024"))
				},
			},
		},
		"failed_database_error": {
			request: &coupons_gen.CreateCouponRequest{
				Title:       "-30% на крем от загара",
				Description: nil,
				Promocode:   "LETO2024",
			},
			expected: expectation{
				response: nil,
				err:      errs.NewErrorBadQuery("rpc error: code = Internal desc = uc coupon create conflict check"),
				sql: func() {
					mock.
						ExpectQuery("SELECT uuid, title, description, promocode FROM coupons WHERE promocode = ?").
						WithArgs("LETO2024").
						WillReturnError(errors.New("connect error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			coupon, err := client.CreateCoupon(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if _, err := uuid.Parse(coupon.Id); err != nil ||
					tt.expected.response.Title != coupon.Title ||
					tt.expected.response.Description != coupon.Description ||
					tt.expected.response.Promocode != coupon.Promocode {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}

			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("there were unfulfilled expectations: %s", err)
			}
		})
	}
}

func TestCouponsService_GetCoupon(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	client := server(db)
	ctx := context.Background()

	type expectation struct {
		response *coupons_gen.CouponResponse
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  *coupons_gen.GetCouponRequest
		expected expectation
	}{
		"success": {
			request: &coupons_gen.GetCouponRequest{
				Id: "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{
					Id:          "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRow("2e4ead7d-b3e3-4127-aba3-cc52cae8192a", "-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234"),
						)
				},
			},
		},
		"success_without_description": {
			request: &coupons_gen.GetCouponRequest{
				Id: "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{
					Id:          "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRow("2e4ead7d-b3e3-4127-aba3-cc52cae8192a", "-25% на первый и повторные онлайн-заказы", "", "RADUGA1234"),
						)
				},
			},
		},
		"failed_empty_uuid": {
			request: &coupons_gen.GetCouponRequest{
				Id: "",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = invalid uuid: invalid UUID length: 0"),
				sql:      func() {},
			},
		},
		"failed_invalid_uuid": {
			request: &coupons_gen.GetCouponRequest{
				Id: "invalid uuid",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = invalid uuid: invalid UUID length: 12"),
				sql:      func() {},
			},
		},
		"failed_not_found": {
			request: &coupons_gen.GetCouponRequest{
				Id: "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorNotFound("rpc error: code = NotFound desc = coupon not found"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}))
				},
			},
		},
		"failed_sql": {
			request: &coupons_gen.GetCouponRequest{
				Id: "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorBadQuery("rpc error: code = Internal desc = uc coupon get by id"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnError(errors.New("some random error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			coupon, err := client.GetCoupon(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if tt.expected.response.Id != coupon.Id ||
					tt.expected.response.Title != coupon.Title ||
					tt.expected.response.Description != coupon.Description ||
					tt.expected.response.Promocode != coupon.Promocode {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}

			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("there were unfulfilled expectations: %s", err)
			}
		})
	}
}

func TestCouponsService_GetCoupons(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	client := server(db)
	ctx := context.Background()

	type expectation struct {
		response *coupons_gen.CouponListPaginationResponse
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  *coupons_gen.GetCouponsRequest
		expected expectation
	}{
		"success": {
			request: &coupons_gen.GetCouponsRequest{},
			expected: expectation{
				response: &coupons_gen.CouponListPaginationResponse{
					Page:  uint64(1),
					Limit: uint32(10),
					Pages: uint64(1),
					Total: uint64(3),
					Coupons: []*coupons_gen.CouponResponse{
						{
							Id:          "1a37bfa8-1cbc-4327-afb7-9d2591a5800e",
							Title:       "Test-купон-1",
							Description: "Test-Описание-1",
							Promocode:   "Test-промокод-1",
						},
						{
							Id:          "5c73335f-d411-4d55-b39f-4e09df442363",
							Title:       "Test-купон-2",
							Description: "",
							Promocode:   "Test-промокод-2",
						},
						{
							Id:          "9606283a-f9a7-48c5-b45e-13b0e6dee113",
							Title:       "",
							Description: "",
							Promocode:   "",
						},
					},
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons LIMIT 10 OFFSET 0`).
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRows(
									[]driver.Value{"1a37bfa8-1cbc-4327-afb7-9d2591a5800e", "Test-купон-1", "Test-Описание-1", "Test-промокод-1"},
									[]driver.Value{"5c73335f-d411-4d55-b39f-4e09df442363", "Test-купон-2", "", "Test-промокод-2"},
									[]driver.Value{"9606283a-f9a7-48c5-b45e-13b0e6dee113", "", "", ""},
								),
						)

					mock.
						ExpectQuery(`SELECT COUNT\(\*\) AS total FROM coupons`).
						WillReturnRows(sqlmock.NewRows([]string{"total"}).AddRow("3"))
				},
			},
		},
		"success_full": {
			request: &coupons_gen.GetCouponsRequest{
				Page:  helper.PUint64(uint64(2)),
				Limit: helper.PUint32(2),
				Title: helper.PStr("Тест"),
			},
			expected: expectation{
				response: &coupons_gen.CouponListPaginationResponse{
					Page:  uint64(2),
					Limit: uint32(2),
					Pages: uint64(5),
					Total: uint64(9),
					Coupons: []*coupons_gen.CouponResponse{
						{
							Id:          "1a37bfa8-1cbc-4327-afb7-9d2591a5800e",
							Title:       "Test-купон-1",
							Description: "Test-Описание-1",
							Promocode:   "Test-промокод-1",
						},
						{
							Id:          "5c73335f-d411-4d55-b39f-4e09df442363",
							Title:       "Test-купон-2",
							Description: "",
							Promocode:   "Test-промокод-2",
						},
					},
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE title LIKE \? LIMIT 2 OFFSET 2`).
						WithArgs("%Тест%").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRows(
									[]driver.Value{"1a37bfa8-1cbc-4327-afb7-9d2591a5800e", "Test-купон-1", "Test-Описание-1", "Test-промокод-1"},
									[]driver.Value{"5c73335f-d411-4d55-b39f-4e09df442363", "Test-купон-2", "", "Test-промокод-2"},
								),
						)

					mock.
						ExpectQuery(`SELECT COUNT\(\*\) AS total FROM coupons`).
						WillReturnRows(sqlmock.NewRows([]string{"total"}).AddRow("9"))
				},
			},
		},
		"success_empty": {
			request: &coupons_gen.GetCouponsRequest{},
			expected: expectation{
				response: &coupons_gen.CouponListPaginationResponse{
					Page:    uint64(1),
					Limit:   uint32(10),
					Pages:   uint64(1),
					Total:   uint64(0),
					Coupons: []*coupons_gen.CouponResponse{},
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons LIMIT 10 OFFSET 0`).
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}))

					mock.
						ExpectQuery(`SELECT COUNT\(\*\) AS total FROM coupons`).
						WillReturnRows(sqlmock.NewRows([]string{"total"}).AddRow("0"))
				},
			},
		},
		"failed_invalid_title": {
			request: &coupons_gen.GetCouponsRequest{
				Title: helper.PStr("Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест Тест"),
			},
			expected: expectation{
				response: &coupons_gen.CouponListPaginationResponse{},
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = Title is too long"),
				sql:      func() {},
			},
		},
		"failed_sql": {
			request: &coupons_gen.GetCouponsRequest{},
			expected: expectation{
				response: &coupons_gen.CouponListPaginationResponse{},
				err:      errs.NewErrorBadQuery("rpc error: code = Internal desc = uc coupon list"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons LIMIT 10 OFFSET 0`).
						WillReturnError(errors.New("some random error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			couponPage, err := client.GetCoupons(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if len(couponPage.Coupons) != len(tt.expected.response.Coupons) ||
					tt.expected.response.Page != couponPage.Page ||
					tt.expected.response.Pages != couponPage.Pages ||
					tt.expected.response.Total != couponPage.Total ||
					tt.expected.response.Limit != couponPage.Limit {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, couponPage)
				} else {
					for i, coupon := range couponPage.Coupons {
						expCoupon := tt.expected.response.Coupons[i]
						if expCoupon.Id != coupon.Id ||
							expCoupon.Title != coupon.Title ||
							expCoupon.Description != coupon.Description ||
							expCoupon.Promocode != coupon.Promocode {
							t.Errorf("Out -> \nWant: %q\nGot : %q", expCoupon, coupon)
						}
					}
				}
			}

			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("there were unfulfilled expectations: %s", err)
			}
		})
	}
}

func TestCouponsService_EditCoupon(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	client := server(db)
	ctx := context.Background()

	type expectation struct {
		response *coupons_gen.CouponResponse
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  *coupons_gen.EditCouponRequest
		expected expectation
	}{
		"success": {
			request: &coupons_gen.EditCouponRequest{
				Id:          "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
				Title:       helper.PStr("-25% на первый и повторные онлайн-заказы"),
				Description: helper.PStr("Небольшое описание"),
				Promocode:   helper.PStr("RADUGA1234"),
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{
					Id:          "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE promocode = \?`).
						WithArgs("RADUGA1234").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRow("2e4ead7d-b3e3-4127-aba3-cc52cae8192a", "-24% на первый и повторные онлайн-заказы", "", "RADUGA1234"),
						)

					mock.
						ExpectExec(`UPDATE coupons SET title = \?, description = \?, promocode = \? WHERE uuid = \?`).
						WithArgs("-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234", "2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnResult(sqlmock.NewResult(1, 1))

					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRow("2e4ead7d-b3e3-4127-aba3-cc52cae8192a", "-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234"),
						)
				},
			},
		},
		"failed_empty_uuid": {
			request: &coupons_gen.EditCouponRequest{
				Id:          "",
				Title:       helper.PStr("-25% на первый и повторные онлайн-заказы"),
				Description: helper.PStr("Небольшое описание"),
				Promocode:   helper.PStr("RADUGA1234"),
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = invalid uuid: invalid UUID length: 0"),
				sql:      func() {},
			},
		},
		"failed_invalid_uuid": {
			request: &coupons_gen.EditCouponRequest{
				Id:          "invalid uuid",
				Title:       helper.PStr("-25% на первый и повторные онлайн-заказы"),
				Description: helper.PStr("Небольшое описание"),
				Promocode:   helper.PStr("RADUGA1234"),
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = invalid uuid: invalid UUID length: 12"),
				sql:      func() {},
			},
		},
		"failed_empty_data": {
			request: &coupons_gen.EditCouponRequest{
				Id: "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = at least one updated field must be filled in"),
				sql:      func() {},
			},
		},
		"failed_not_found": {
			request: &coupons_gen.EditCouponRequest{
				Id:        "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
				Title:     helper.PStr("-25% на первый и повторные онлайн-заказы"),
				Promocode: helper.PStr("RADUGA1234"),
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorNotFound("rpc error: code = NotFound desc = coupon not found"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE promocode = \?`).
						WithArgs("RADUGA1234").
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}))

					mock.
						ExpectExec(`UPDATE coupons SET title = \?, promocode = \? WHERE uuid = \?`).
						WithArgs("-25% на первый и повторные онлайн-заказы", "RADUGA1234", "2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnResult(sqlmock.NewResult(1, 0))
				},
			},
		},
		"failed_sql": {
			request: &coupons_gen.EditCouponRequest{
				Id:          "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
				Description: helper.PStr("Небольшое описание"),
			},
			expected: expectation{
				response: &coupons_gen.CouponResponse{},
				err:      errs.NewErrorBadQuery("rpc error: code = Internal desc = uc coupon update"),
				sql: func() {
					mock.
						ExpectExec(`UPDATE coupons SET description = \? WHERE uuid = \?`).
						WithArgs("Небольшое описание", "2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnError(errors.New("some random error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			coupon, err := client.EditCoupon(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if tt.expected.response.Id != coupon.Id ||
					tt.expected.response.Title != coupon.Title ||
					tt.expected.response.Description != coupon.Description ||
					tt.expected.response.Promocode != coupon.Promocode {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}

			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("there were unfulfilled expectations: %s", err)
			}
		})
	}
}

func TestCouponsService_DeleteCoupon(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	client := server(db)
	ctx := context.Background()

	type expectation struct {
		response *coupons_gen.DeletedResponse
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  *coupons_gen.DeleteCouponRequest
		expected expectation
	}{
		"success": {
			request: &coupons_gen.DeleteCouponRequest{
				Id: "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
			},
			expected: expectation{
				response: &coupons_gen.DeletedResponse{
					Success: true,
				},
				err: nil,
				sql: func() {
					mock.
						ExpectExec(`DELETE FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnResult(sqlmock.NewResult(1, 1))
				},
			},
		},
		"failed_empty_uuid": {
			request: &coupons_gen.DeleteCouponRequest{
				Id: "",
			},
			expected: expectation{
				response: &coupons_gen.DeletedResponse{},
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = invalid uuid: invalid UUID length: 0"),
				sql:      func() {},
			},
		},
		"failed_invalid_uuid": {
			request: &coupons_gen.DeleteCouponRequest{
				Id: "invalid uuid",
			},
			expected: expectation{
				response: &coupons_gen.DeletedResponse{},
				err:      errs.NewErrorInvalidData("rpc error: code = InvalidArgument desc = invalid uuid: invalid UUID length: 12"),
				sql:      func() {},
			},
		},
		"failed_not_found": {
			request: &coupons_gen.DeleteCouponRequest{
				Id: "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
			},
			expected: expectation{
				response: &coupons_gen.DeletedResponse{},
				err:      errs.NewErrorNotFound("rpc error: code = NotFound desc = coupon not found"),
				sql: func() {
					mock.
						ExpectExec(`DELETE FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnResult(sqlmock.NewResult(1, 0))
				},
			},
		},
		"failed_sql": {
			request: &coupons_gen.DeleteCouponRequest{
				Id: "2e4ead7d-b3e3-4127-aba3-cc52cae8192a",
			},
			expected: expectation{
				response: &coupons_gen.DeletedResponse{},
				err:      errs.NewErrorBadQuery("rpc error: code = Internal desc = uc coupon delete"),
				sql: func() {
					mock.
						ExpectExec(`DELETE FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnError(errors.New("some random error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			coupon, err := client.DeleteCoupon(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if tt.expected.response.Success != coupon.Success {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}

			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("there were unfulfilled expectations: %s", err)
			}
		})
	}
}
