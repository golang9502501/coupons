package controller

import (
	"context"
	"coupons/coupons/coupons_gen"
	"coupons/coupons/dto"
	"coupons/coupons/usecase"
	"coupons/internal/validator"
)

const (
	defLimit uint32 = 10
)

type CouponController struct {
	coupons_gen.UnimplementedGrpcCouponsServiceServer
	validator *validator.Validator
	uc        usecase.CouponUseCaseInterface
}

func NewCouponController(uc usecase.CouponUseCaseInterface, validator *validator.Validator) CouponController {
	return CouponController{validator: validator, uc: uc}
}

func (c *CouponController) CreateCoupon(ctx context.Context, request *coupons_gen.CreateCouponRequest) (*coupons_gen.CouponResponse, error) {

	requestDto, err := dto.NewCreateCouponRequest(request).ToEntity()
	if err != nil {
		return nil, err
	}

	if err := requestDto.Validate(c.validator); err != nil {
		return nil, err
	}

	responseDto, err := c.uc.Create(ctx, requestDto)
	if err != nil {
		return nil, err
	}

	responseDtoGrpc, err := responseDto.ToGrpc()
	if err != nil {
		return nil, err
	}

	return responseDtoGrpc, nil
}

func (c *CouponController) GetCoupon(ctx context.Context, request *coupons_gen.GetCouponRequest) (*coupons_gen.CouponResponse, error) {

	uuidVal, err := dto.NewGetCouponRequest(request).ToUuid()
	if err != nil {
		return nil, err
	}

	responseDto, err := c.uc.GetByUuid(ctx, uuidVal)
	if err != nil {
		return nil, err
	}

	responseDtoGrpc, err := responseDto.ToGrpc()
	if err != nil {
		return nil, err
	}

	return responseDtoGrpc, nil
}

func (c *CouponController) GetCoupons(ctx context.Context, request *coupons_gen.GetCouponsRequest) (*coupons_gen.CouponListPaginationResponse, error) {

	requestDto, err := dto.NewGetCouponsRequest(request).ToEntity(defLimit)
	if err != nil {
		return nil, err
	}

	if err := requestDto.Validate(c.validator); err != nil {
		return nil, err
	}

	responseDto, err := c.uc.GetList(ctx, requestDto)
	if err != nil {
		return nil, err
	}

	responseDtoGrpc, err := responseDto.ToGrpc()
	if err != nil {
		return nil, err
	}

	return responseDtoGrpc, nil
}

func (c *CouponController) EditCoupon(ctx context.Context, request *coupons_gen.EditCouponRequest) (*coupons_gen.CouponResponse, error) {

	requestDto, err := dto.NewEditCouponRequest(request).ToEntity()
	if err != nil {
		return nil, err
	}

	if err := requestDto.Validate(c.validator); err != nil {
		return nil, err
	}

	responseDto, err := c.uc.Update(ctx, requestDto)
	if err != nil {
		return nil, err
	}

	responseDtoGrpc, err := responseDto.ToGrpc()
	if err != nil {
		return nil, err
	}

	return responseDtoGrpc, nil
}

func (c *CouponController) DeleteCoupon(ctx context.Context, request *coupons_gen.DeleteCouponRequest) (*coupons_gen.DeletedResponse, error) {

	uuidVal, err := dto.NewDeleteCouponRequest(request).ToUuid()
	if err != nil {
		return nil, err
	}

	if err := c.uc.Delete(ctx, uuidVal); err != nil {
		return nil, err
	}

	return &coupons_gen.DeletedResponse{Success: true}, nil
}
