package usecase

import (
	"context"
	"coupons/coupons/dto"
	"coupons/coupons/errs"
	"coupons/coupons/repository"
	"github.com/google/uuid"
	"math"
)

type CouponUseCase struct {
	r repository.RepoInterface
}

func NewCouponUseCase(r repository.RepoInterface) CouponUseCase {
	return CouponUseCase{r}
}

func (uc CouponUseCase) Create(ctx context.Context, couponReq dto.CreateCoupon) (dto.Coupon, error) {

	_, err := uc.r.GetByPromocode(ctx, couponReq.Promocode)

	if err == nil {
		return dto.Coupon{}, errs.NewErrorAlreadyExists("coupon with this promocode already exists")
	} else {
		if !errs.IsErrorNotFound(err) {
			return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon create conflict check")
		}
	}

	uuidVal, err := uc.r.Create(ctx, couponReq)

	if err != nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon create")
	}

	couponResp, err := uc.r.GetByUuid(ctx, uuidVal)

	if err != nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon create existence check")
	}

	return couponResp, nil
}

func (uc CouponUseCase) Update(ctx context.Context, couponReq dto.EditCoupon) (dto.Coupon, error) {

	if couponReq.Uuid == uuid.Nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon update empty uuid")
	}

	if couponReq.Title == nil && couponReq.Description == nil && couponReq.Promocode == nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon update empty data")
	}

	if couponReq.Promocode != nil {
		couponDup, err := uc.r.GetByPromocode(ctx, *couponReq.Promocode)

		if err == nil {
			if couponReq.Uuid.String() != couponDup.Uuid.String() {
				return dto.Coupon{}, errs.NewErrorAlreadyExists("coupon with this promocode already exists")
			}
		} else {
			if !errs.IsErrorNotFound(err) {
				return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon update conflict check")
			}
		}
	}

	if err := uc.r.Update(ctx, couponReq); err != nil {
		if errs.IsErrorNotFound(err) {
			return dto.Coupon{}, err
		}
		return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon update")
	}

	couponResp, err := uc.r.GetByUuid(ctx, couponReq.Uuid)
	if err != nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon update existence check")
	}

	return couponResp, nil
}

func (uc CouponUseCase) GetList(ctx context.Context, params dto.GetCoupons) (dto.CouponListPagination, error) {

	if params.Page == 0 || params.Limit == 0 {
		return dto.CouponListPagination{}, errs.NewErrorBadQuery("uc coupon list empty pagination")
	}

	couponsResp, err := uc.r.GetList(ctx, params)

	if err != nil {
		return dto.CouponListPagination{}, errs.NewErrorBadQuery("uc coupon list")
	}

	totalResp, err := uc.r.GetListTotal(ctx, params)

	if err != nil {
		return dto.CouponListPagination{}, errs.NewErrorBadQuery("uc coupon list count")
	}

	pages := uint64(math.Ceil(float64(totalResp) / float64(params.Limit)))
	if pages < 1 {
		pages = 1
	}

	pgn := dto.CouponListPagination{
		Page:  params.Page,
		Limit: params.Limit,
		Pages: pages,
		Total: totalResp,
		List:  couponsResp,
	}

	return pgn, nil
}

func (uc CouponUseCase) GetByUuid(ctx context.Context, uuidVal uuid.UUID) (dto.Coupon, error) {

	if uuidVal == uuid.Nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon get by id empty uuid")
	}

	couponResp, err := uc.r.GetByUuid(ctx, uuidVal)

	if err != nil {
		if errs.IsErrorNotFound(err) {
			return dto.Coupon{}, err
		}
		return dto.Coupon{}, errs.NewErrorBadQuery("uc coupon get by id")
	}

	return couponResp, nil
}

func (uc CouponUseCase) Delete(ctx context.Context, uuidVal uuid.UUID) error {

	if uuidVal == uuid.Nil {
		return errs.NewErrorBadQuery("uc coupon delete empty uuid")
	}

	if err := uc.r.DeleteByUuid(ctx, uuidVal); err != nil {
		if errs.IsErrorNotFound(err) {
			return err
		}
		return errs.NewErrorBadQuery("uc coupon delete")
	}

	return nil
}
