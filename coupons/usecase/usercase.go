package usecase

import (
	"context"
	"coupons/coupons/dto"
	"github.com/google/uuid"
)

type CouponUseCaseInterface interface {
	Create(ctx context.Context, couponReq dto.CreateCoupon) (dto.Coupon, error)
	Update(ctx context.Context, couponReq dto.EditCoupon) (dto.Coupon, error)
	GetList(ctx context.Context, params dto.GetCoupons) (dto.CouponListPagination, error)
	GetByUuid(ctx context.Context, uuid uuid.UUID) (dto.Coupon, error)
	Delete(ctx context.Context, uuid uuid.UUID) error
}
