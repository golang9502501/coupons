package usecase

import (
	"context"
	"coupons/coupons/dto"
	"coupons/coupons/errs"
	"coupons/coupons/repository"
	"coupons/internal/helper"
	"errors"
	"github.com/google/uuid"
	"go.uber.org/mock/gomock"
	"testing"
)

func useCase(t *testing.T) (CouponUseCaseInterface, *repository.MockRepoInterface) {
	ctrl := gomock.NewController(t)
	t.Cleanup(func() {
		ctrl.Finish()
	})

	repo := repository.NewMockRepoInterface(ctrl)
	return NewCouponUseCase(repo), repo
}

func TestCouponUseCase_Create(t *testing.T) {
	uc, repo := useCase(t)
	ctx := context.Background()

	type expectation struct {
		response dto.Coupon
		err      error
		mock     func()
	}

	tests := map[string]struct {
		request  dto.CreateCoupon
		expected expectation
	}{
		"success": {
			request: dto.CreateCoupon{
				Title:       "-25% на первый и повторные онлайн-заказы",
				Description: "Небольшое описание",
				Promocode:   "RADUGA1234",
			},
			expected: expectation{
				response: dto.Coupon{
					Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{}, errs.NewErrorNotFound("coupon not found"))

					repo.
						EXPECT().
						Create(gomock.Any(), dto.CreateCoupon{
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}).
						Times(1).
						Return(uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"), nil)

					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{
							Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}, nil)
				},
			},
		},
		"failed_duplicated_coupon": {
			request: dto.CreateCoupon{
				Title:       "-25% на первый и повторные онлайн-заказы",
				Description: "Небольшое описание",
				Promocode:   "RADUGA1234",
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorAlreadyExists("coupon with this promocode already exists"),
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{
							Uuid:        uuid.MustParse("1f8a6658-28cc-4f17-b008-52e0c7fa040b"),
							Title:       "-30% на второй и повторные онлайн-заказы",
							Description: "Небольшое описание для купона с таким же промокодом",
							Promocode:   "RADUGA1234",
						}, nil)
				},
			},
		},
		"failed_get_duplicated_coupon": {
			request: dto.CreateCoupon{
				Title:       "-25% на первый и повторные онлайн-заказы",
				Description: "Небольшое описание",
				Promocode:   "RADUGA1234",
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon create conflict check"),
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{}, errors.New("some random error"))
				},
			},
		},
		"failed_create": {
			request: dto.CreateCoupon{
				Title:       "-25% на первый и повторные онлайн-заказы",
				Description: "Небольшое описание",
				Promocode:   "RADUGA1234",
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon create"),
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{}, errs.NewErrorNotFound("coupon not found"))

					repo.
						EXPECT().
						Create(gomock.Any(), dto.CreateCoupon{
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}).
						Times(1).
						Return(uuid.UUID{}, errors.New("some random error"))
				},
			},
		},
		"failed_get_by_uuid": {
			request: dto.CreateCoupon{
				Title:       "-25% на первый и повторные онлайн-заказы",
				Description: "Небольшое описание",
				Promocode:   "RADUGA1234",
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon create existence check"),
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{}, errs.NewErrorNotFound("coupon not found"))

					repo.
						EXPECT().
						Create(gomock.Any(), dto.CreateCoupon{
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}).
						Times(1).
						Return(uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"), nil)

					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{}, errors.New("some random error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.mock()
			coupon, err := uc.Create(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if coupon.Uuid.String() != coupon.Uuid.String() ||
					tt.expected.response.Title != coupon.Title ||
					tt.expected.response.Description != coupon.Description ||
					tt.expected.response.Promocode != coupon.Promocode {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}
		})
	}
}

func TestCouponUseCase_Update(t *testing.T) {
	uc, repo := useCase(t)
	ctx := context.Background()

	type expectation struct {
		response dto.Coupon
		err      error
		mock     func()
	}

	tests := map[string]struct {
		request  dto.EditCoupon
		expected expectation
	}{
		"success_with_new_promocode": {
			request: dto.EditCoupon{
				Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title:       helper.PStr("-25% на первый и повторные онлайн-заказы"),
				Description: helper.PStr("Небольшое описание"),
				Promocode:   helper.PStr("RADUGA1234"),
			},
			expected: expectation{
				response: dto.Coupon{
					Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{}, errs.NewErrorNotFound("coupon not found"))

					repo.
						EXPECT().
						Update(gomock.Any(), dto.EditCoupon{
							Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title:       helper.PStr("-25% на первый и повторные онлайн-заказы"),
							Description: helper.PStr("Небольшое описание"),
							Promocode:   helper.PStr("RADUGA1234"),
						}).
						Times(1).
						Return(nil)

					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{
							Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}, nil)
				},
			},
		},
		"success_with_old_promocode": {
			request: dto.EditCoupon{
				Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title:       helper.PStr("-25% на первый и повторные онлайн-заказы"),
				Description: helper.PStr("Небольшое описание"),
				Promocode:   helper.PStr("RADUGA1234"),
			},
			expected: expectation{
				response: dto.Coupon{
					Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{
							Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}, nil)

					repo.
						EXPECT().
						Update(gomock.Any(), dto.EditCoupon{
							Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title:       helper.PStr("-25% на первый и повторные онлайн-заказы"),
							Description: helper.PStr("Небольшое описание"),
							Promocode:   helper.PStr("RADUGA1234"),
						}).
						Times(1).
						Return(nil)

					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{
							Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}, nil)
				},
			},
		},
		"success_without_promocode": {
			request: dto.EditCoupon{
				Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
			},
			expected: expectation{
				response: dto.Coupon{
					Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
					Title:       "-26% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						Update(gomock.Any(), dto.EditCoupon{
							Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
						}).
						Times(1).
						Return(nil)

					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{
							Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title:       "-26% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}, nil)
				},
			},
		},
		"failed_empty_uuid": {
			request: dto.EditCoupon{
				Uuid:  uuid.UUID{},
				Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon update empty uuid"),
				mock:     func() {},
			},
		},
		"failed_empty_data": {
			request: dto.EditCoupon{
				Uuid: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon update empty data"),
				mock:     func() {},
			},
		},
		"failed_promocode_already_exists": {
			request: dto.EditCoupon{
				Uuid:      uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Promocode: helper.PStr("RADUGA1234"),
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorAlreadyExists("coupon with this promocode already exists"),
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{
							Uuid:        uuid.MustParse("1f8a6658-28cc-4f17-b008-52e0c7fa040b"),
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}, nil)
				},
			},
		},
		"failed_get_promocode": {
			request: dto.EditCoupon{
				Uuid:      uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Promocode: helper.PStr("RADUGA1234"),
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon update conflict check"),
				mock: func() {
					repo.
						EXPECT().
						GetByPromocode(gomock.Any(), "RADUGA1234").
						Times(1).
						Return(dto.Coupon{}, errors.New("some random error"))
				},
			},
		},
		"failed_update_not_found": {
			request: dto.EditCoupon{
				Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorNotFound("coupon not found"),
				mock: func() {
					repo.
						EXPECT().
						Update(gomock.Any(), dto.EditCoupon{
							Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
						}).
						Times(1).
						Return(errs.NewErrorNotFound("coupon not found"))
				},
			},
		},
		"failed_update": {
			request: dto.EditCoupon{
				Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon update"),
				mock: func() {
					repo.
						EXPECT().
						Update(gomock.Any(), dto.EditCoupon{
							Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
						}).
						Times(1).
						Return(errors.New("some random error"))
				},
			},
		},
		"failed_check_exists": {
			request: dto.EditCoupon{
				Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
			},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon update existence check"),
				mock: func() {
					repo.
						EXPECT().
						Update(gomock.Any(), dto.EditCoupon{
							Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title: helper.PStr("-26% на первый и повторные онлайн-заказы"),
						}).
						Times(1).
						Return(nil)

					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{}, errors.New("some random error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.mock()
			coupon, err := uc.Update(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if coupon.Uuid.String() != coupon.Uuid.String() ||
					tt.expected.response.Title != coupon.Title ||
					tt.expected.response.Description != coupon.Description ||
					tt.expected.response.Promocode != coupon.Promocode {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}
		})
	}
}

func TestCouponUseCase_GetList(t *testing.T) {
	uc, repo := useCase(t)
	ctx := context.Background()

	type expectation struct {
		response dto.CouponListPagination
		err      error
		mock     func()
	}

	tests := map[string]struct {
		request  dto.GetCoupons
		expected expectation
	}{
		"success": {
			request: dto.GetCoupons{
				Page:  1,
				Limit: 20,
				Title: "Test",
			},
			expected: expectation{
				response: dto.CouponListPagination{
					Page:  1,
					Limit: 20,
					Total: 2,
					Pages: 1,
					List: []dto.Coupon{
						{
							Uuid:        uuid.New(),
							Title:       "Купон 1",
							Description: "Описание 1",
							Promocode:   "промокод1",
						},
						{
							Uuid:        uuid.New(),
							Title:       "Купон 2",
							Description: "Описание 2",
							Promocode:   "промокод2",
						},
					},
				},
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						GetList(gomock.Any(), dto.GetCoupons{
							Page:  1,
							Limit: 20,
							Title: "Test",
						}).
						Times(1).
						Return([]dto.Coupon{
							{
								Uuid:        uuid.New(),
								Title:       "Купон 1",
								Description: "Описание 1",
								Promocode:   "промокод1",
							},
							{
								Uuid:        uuid.New(),
								Title:       "Купон 2",
								Description: "Описание 2",
								Promocode:   "промокод2",
							},
						}, nil)

					repo.
						EXPECT().
						GetListTotal(gomock.Any(), dto.GetCoupons{
							Page:  1,
							Limit: 20,
							Title: "Test",
						}).
						Times(1).
						Return(uint64(2), nil)
				},
			},
		},
		"success_empty_list": {
			request: dto.GetCoupons{
				Page:  3,
				Limit: 5,
			},
			expected: expectation{
				response: dto.CouponListPagination{
					Page:  3,
					Limit: 5,
					Total: 2,
					Pages: 1,
					List:  []dto.Coupon{},
				},
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						GetList(gomock.Any(), dto.GetCoupons{
							Page:  3,
							Limit: 5,
						}).
						Times(1).
						Return([]dto.Coupon{}, nil)

					repo.
						EXPECT().
						GetListTotal(gomock.Any(), dto.GetCoupons{
							Page:  3,
							Limit: 5,
						}).
						Times(1).
						Return(uint64(2), nil)
				},
			},
		},
		"success_second_page": {
			request: dto.GetCoupons{
				Page:  2,
				Limit: 2,
			},
			expected: expectation{
				response: dto.CouponListPagination{
					Page:  2,
					Limit: 2,
					Total: 11,
					Pages: 6,
					List: []dto.Coupon{
						{
							Uuid:        uuid.New(),
							Title:       "Купон 1",
							Description: "Описание 1",
							Promocode:   "промокод1",
						},
						{
							Uuid:        uuid.New(),
							Title:       "Купон 2",
							Description: "Описание 2",
							Promocode:   "промокод2",
						},
					},
				},
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						GetList(gomock.Any(), dto.GetCoupons{
							Page:  2,
							Limit: 2,
						}).
						Times(1).
						Return([]dto.Coupon{
							{
								Uuid:        uuid.New(),
								Title:       "Купон 1",
								Description: "Описание 1",
								Promocode:   "промокод1",
							},
							{
								Uuid:        uuid.New(),
								Title:       "Купон 2",
								Description: "Описание 2",
								Promocode:   "промокод2",
							},
						}, nil)

					repo.
						EXPECT().
						GetListTotal(gomock.Any(), dto.GetCoupons{
							Page:  2,
							Limit: 2,
						}).
						Times(1).
						Return(uint64(11), nil)
				},
			},
		},
		"failed_empty_data": {
			request: dto.GetCoupons{
				Title: "some title",
			},
			expected: expectation{
				response: dto.CouponListPagination{},
				err:      errs.NewErrorBadQuery("uc coupon list empty pagination"),
				mock:     func() {},
			},
		},
		"failed_get_list": {
			request: dto.GetCoupons{
				Page:  2,
				Limit: 2,
			},
			expected: expectation{
				response: dto.CouponListPagination{},
				err:      errs.NewErrorBadQuery("uc coupon list"),
				mock: func() {
					repo.
						EXPECT().
						GetList(gomock.Any(), dto.GetCoupons{
							Page:  2,
							Limit: 2,
						}).
						Times(1).
						Return([]dto.Coupon{}, errors.New("some random errors"))
				},
			},
		},
		"failed_get_list_total": {
			request: dto.GetCoupons{
				Page:  1,
				Limit: 20,
			},
			expected: expectation{
				response: dto.CouponListPagination{},
				err:      errs.NewErrorBadQuery("uc coupon list count"),
				mock: func() {
					repo.
						EXPECT().
						GetList(gomock.Any(), dto.GetCoupons{
							Page:  1,
							Limit: 20,
						}).
						Times(1).
						Return([]dto.Coupon{
							{
								Uuid:        uuid.New(),
								Title:       "Купон 1",
								Description: "Описание 1",
								Promocode:   "промокод1",
							},
						}, nil)

					repo.
						EXPECT().
						GetListTotal(gomock.Any(), dto.GetCoupons{
							Page:  1,
							Limit: 20,
						}).
						Times(1).
						Return(uint64(0), errors.New("some random errors"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.mock()
			couponPage, err := uc.GetList(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if len(couponPage.List) != len(tt.expected.response.List) ||
					tt.expected.response.Page != couponPage.Page ||
					tt.expected.response.Pages != couponPage.Pages ||
					tt.expected.response.Total != couponPage.Total ||
					tt.expected.response.Limit != couponPage.Limit {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, couponPage)
				} else {
					for i, coupon := range couponPage.List {
						expCoupon := tt.expected.response.List[i]
						if coupon.Uuid == uuid.Nil ||
							expCoupon.Title != coupon.Title ||
							expCoupon.Description != coupon.Description ||
							expCoupon.Promocode != coupon.Promocode {
							t.Errorf("Out -> \nWant: %q\nGot : %q", expCoupon, coupon)
						}
					}
				}
			}
		})
	}
}

func TestCouponUseCase_GetByUuid(t *testing.T) {
	uc, repo := useCase(t)
	ctx := context.Background()

	type expectation struct {
		response dto.Coupon
		err      error
		mock     func()
	}

	tests := map[string]struct {
		request  uuid.UUID
		expected expectation
	}{
		"success": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				response: dto.Coupon{
					Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{
							Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
							Title:       "-25% на первый и повторные онлайн-заказы",
							Description: "Небольшое описание",
							Promocode:   "RADUGA1234",
						}, nil)
				},
			},
		},
		"failed_empty_uuid": {
			request: uuid.UUID{},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon get by id empty uuid"),
				mock:     func() {},
			},
		},
		"failed_not_found": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorNotFound("coupon not found"),
				mock: func() {
					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{}, errs.NewErrorNotFound("coupon not found"))
				},
			},
		},
		"failed_get_by_uuid": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("uc coupon get by id"),
				mock: func() {
					repo.
						EXPECT().
						GetByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(dto.Coupon{}, errors.New("random coupon error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.mock()
			coupon, err := uc.GetByUuid(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if coupon.Uuid.String() != coupon.Uuid.String() ||
					tt.expected.response.Title != coupon.Title ||
					tt.expected.response.Description != coupon.Description ||
					tt.expected.response.Promocode != coupon.Promocode {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}
		})
	}
}

func TestCouponUseCase_Delete(t *testing.T) {
	uc, repo := useCase(t)
	ctx := context.Background()

	type expectation struct {
		err  error
		mock func()
	}

	tests := map[string]struct {
		request  uuid.UUID
		expected expectation
	}{
		"success": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				err: nil,
				mock: func() {
					repo.
						EXPECT().
						DeleteByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(nil)
				},
			},
		},
		"failed_empty_uuid": {
			request: uuid.UUID{},
			expected: expectation{
				err:  errs.NewErrorBadQuery("uc coupon delete empty uuid"),
				mock: func() {},
			},
		},
		"failed_not_found": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				err: errs.NewErrorNotFound("coupon not found"),
				mock: func() {
					repo.
						EXPECT().
						DeleteByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(errs.NewErrorNotFound("coupon not found"))
				},
			},
		},
		"failed_delete": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				err: errs.NewErrorBadQuery("uc coupon delete"),
				mock: func() {
					repo.
						EXPECT().
						DeleteByUuid(gomock.Any(), uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e")).
						Times(1).
						Return(errors.New("some random error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.mock()
			if errT := helper.TestCompErrs(uc.Delete(ctx, tt.request), tt.expected.err); errT != nil {
				t.Error(errT.Error())
			}
		})
	}
}
