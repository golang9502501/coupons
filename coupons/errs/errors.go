package errs

import (
	"errors"
	"github.com/rs/zerolog"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ErrorAlreadyExists struct {
	msg string
}

func NewErrorAlreadyExists(msg string) ErrorAlreadyExists {
	return ErrorAlreadyExists{msg: msg}
}

func (e ErrorAlreadyExists) Error() string {
	return e.msg
}

func (e ErrorAlreadyExists) GRPCStatus() *status.Status {
	return status.New(codes.AlreadyExists, e.Error())
}

type ErrorNotFound struct {
	msg string
}

func NewErrorNotFound(msg string) ErrorNotFound {
	return ErrorNotFound{msg: msg}
}

func IsErrorNotFound(err error) bool {
	var errorNotFound ErrorNotFound
	return errors.As(err, &errorNotFound)
}

func (e ErrorNotFound) Error() string {
	return e.msg
}

func (e ErrorNotFound) GRPCStatus() *status.Status {
	return status.New(codes.NotFound, e.Error())
}

type ErrorBadQuery struct {
	msg string
}

func NewErrorBadQuery(msg string) ErrorBadQuery {
	return ErrorBadQuery{msg: msg}
}

func (e ErrorBadQuery) Error() string {
	return e.msg
}

func (e ErrorBadQuery) GRPCStatus() *status.Status {
	return status.New(codes.Internal, e.Error())
}

func (e ErrorBadQuery) WithLog(event *zerolog.Event) ErrorBadQuery {
	event.Msg(e.msg)
	return e
}

type ErrorInvalidData struct {
	msg string
}

func NewErrorInvalidData(msg string) ErrorInvalidData {
	return ErrorInvalidData{msg: msg}
}

func (e ErrorInvalidData) Error() string {
	return e.msg
}

func (e ErrorInvalidData) GRPCStatus() *status.Status {
	return status.New(codes.InvalidArgument, e.Error())
}
