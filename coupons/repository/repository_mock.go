// Code generated by MockGen. DO NOT EDIT.
// Source: coupons/repository/repository.go
//
// Generated by this command:
//
//	mockgen -source coupons/repository/repository.go -destination coupons/repository/repository_mock.go -package repository
//

// Package repository is a generated GoMock package.
package repository

import (
	context "context"
	dto "coupons/coupons/dto"
	reflect "reflect"

	uuid "github.com/google/uuid"
	gomock "go.uber.org/mock/gomock"
)

// MockRepoInterface is a mock of RepoInterface interface.
type MockRepoInterface struct {
	ctrl     *gomock.Controller
	recorder *MockRepoInterfaceMockRecorder
}

// MockRepoInterfaceMockRecorder is the mock recorder for MockRepoInterface.
type MockRepoInterfaceMockRecorder struct {
	mock *MockRepoInterface
}

// NewMockRepoInterface creates a new mock instance.
func NewMockRepoInterface(ctrl *gomock.Controller) *MockRepoInterface {
	mock := &MockRepoInterface{ctrl: ctrl}
	mock.recorder = &MockRepoInterfaceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockRepoInterface) EXPECT() *MockRepoInterfaceMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockRepoInterface) Create(ctx context.Context, coupon dto.CreateCoupon) (uuid.UUID, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", ctx, coupon)
	ret0, _ := ret[0].(uuid.UUID)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Create indicates an expected call of Create.
func (mr *MockRepoInterfaceMockRecorder) Create(ctx, coupon any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockRepoInterface)(nil).Create), ctx, coupon)
}

// DeleteByUuid mocks base method.
func (m *MockRepoInterface) DeleteByUuid(ctx context.Context, uuid uuid.UUID) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteByUuid", ctx, uuid)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteByUuid indicates an expected call of DeleteByUuid.
func (mr *MockRepoInterfaceMockRecorder) DeleteByUuid(ctx, uuid any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteByUuid", reflect.TypeOf((*MockRepoInterface)(nil).DeleteByUuid), ctx, uuid)
}

// GetByPromocode mocks base method.
func (m *MockRepoInterface) GetByPromocode(ctx context.Context, promo string) (dto.Coupon, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByPromocode", ctx, promo)
	ret0, _ := ret[0].(dto.Coupon)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetByPromocode indicates an expected call of GetByPromocode.
func (mr *MockRepoInterfaceMockRecorder) GetByPromocode(ctx, promo any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByPromocode", reflect.TypeOf((*MockRepoInterface)(nil).GetByPromocode), ctx, promo)
}

// GetByUuid mocks base method.
func (m *MockRepoInterface) GetByUuid(ctx context.Context, uuid uuid.UUID) (dto.Coupon, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByUuid", ctx, uuid)
	ret0, _ := ret[0].(dto.Coupon)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetByUuid indicates an expected call of GetByUuid.
func (mr *MockRepoInterfaceMockRecorder) GetByUuid(ctx, uuid any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByUuid", reflect.TypeOf((*MockRepoInterface)(nil).GetByUuid), ctx, uuid)
}

// GetList mocks base method.
func (m *MockRepoInterface) GetList(ctx context.Context, params dto.GetCoupons) ([]dto.Coupon, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetList", ctx, params)
	ret0, _ := ret[0].([]dto.Coupon)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetList indicates an expected call of GetList.
func (mr *MockRepoInterfaceMockRecorder) GetList(ctx, params any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetList", reflect.TypeOf((*MockRepoInterface)(nil).GetList), ctx, params)
}

// GetListTotal mocks base method.
func (m *MockRepoInterface) GetListTotal(ctx context.Context, params dto.GetCoupons) (uint64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetListTotal", ctx, params)
	ret0, _ := ret[0].(uint64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetListTotal indicates an expected call of GetListTotal.
func (mr *MockRepoInterfaceMockRecorder) GetListTotal(ctx, params any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetListTotal", reflect.TypeOf((*MockRepoInterface)(nil).GetListTotal), ctx, params)
}

// Update mocks base method.
func (m *MockRepoInterface) Update(ctx context.Context, coupon dto.EditCoupon) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", ctx, coupon)
	ret0, _ := ret[0].(error)
	return ret0
}

// Update indicates an expected call of Update.
func (mr *MockRepoInterfaceMockRecorder) Update(ctx, coupon any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockRepoInterface)(nil).Update), ctx, coupon)
}
