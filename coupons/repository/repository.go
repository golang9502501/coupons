package repository

import (
	"context"
	"coupons/coupons/dto"
	"github.com/google/uuid"
)

type RepoInterface interface {
	Create(ctx context.Context, coupon dto.CreateCoupon) (uuid.UUID, error)
	Update(ctx context.Context, coupon dto.EditCoupon) error
	GetList(ctx context.Context, params dto.GetCoupons) ([]dto.Coupon, error)
	GetListTotal(ctx context.Context, params dto.GetCoupons) (uint64, error)
	GetByUuid(ctx context.Context, uuid uuid.UUID) (dto.Coupon, error)
	GetByPromocode(ctx context.Context, promo string) (dto.Coupon, error)
	DeleteByUuid(ctx context.Context, uuid uuid.UUID) error
}
