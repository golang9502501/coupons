package repository

import (
	"context"
	"coupons/coupons/dto"
	"coupons/coupons/errs"
	"database/sql"
	"errors"
	"fmt"
	"github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
)

const couponsTable = "coupons"

type CouponMariaDb struct {
	log *zerolog.Logger
	db  *sql.DB
}

func NewCouponMariaDb(db *sql.DB, logSrv *zerolog.Logger) *CouponMariaDb {
	return &CouponMariaDb{log: logSrv, db: db}
}

func (repo *CouponMariaDb) Create(ctx context.Context, coupon dto.CreateCoupon) (uuid.UUID, error) {
	sqlStr, args, err := squirrel.
		Insert(couponsTable).
		Columns("uuid", "title", "description", "promocode").
		Values(uuid.New().String(), coupon.Title, coupon.Description, coupon.Promocode).
		Suffix("RETURNING uuid").
		ToSql()

	if err != nil {
		return uuid.UUID{}, errs.NewErrorBadQuery("repository create coupon sql").WithLog(repo.log.Error().Ctx(ctx).Err(err).Any("coupon", coupon))
	}

	row := repo.db.QueryRowContext(ctx, sqlStr, args...)

	var uuidStr string
	if err := row.Scan(&uuidStr); err != nil {
		return uuid.UUID{}, errs.NewErrorBadQuery("repository create coupon result scan").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	uuidVal, err := uuid.Parse(uuidStr)
	if err != nil {
		return uuid.UUID{}, errs.NewErrorBadQuery("repository create coupon result scan uuid").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("uuid", uuidStr))
	}

	return uuidVal, nil
}

func (repo *CouponMariaDb) Update(ctx context.Context, coupon dto.EditCoupon) error {
	if coupon.Uuid == uuid.Nil {
		return errs.NewErrorBadQuery("repository empty coupon update uuid").WithLog(repo.log.Error().Ctx(ctx))
	}

	if coupon.Title == nil && coupon.Description == nil && coupon.Promocode == nil {
		return errs.NewErrorBadQuery("repository empty coupon update data").WithLog(repo.log.Error().Ctx(ctx))
	}

	builder := squirrel.
		Update(couponsTable).
		Where(squirrel.Eq{"uuid": coupon.Uuid.String()})

	if coupon.Title != nil {
		builder = builder.Set("title", *coupon.Title)
	}

	if coupon.Description != nil {
		builder = builder.Set("description", *coupon.Description)
	}

	if coupon.Promocode != nil {
		builder = builder.Set("promocode", *coupon.Promocode)
	}

	sqlStr, args, err := builder.ToSql()

	if err != nil {
		return errs.NewErrorBadQuery("repository update coupon sql").WithLog(repo.log.Error().Ctx(ctx).Err(err).Any("coupon", coupon))
	}

	res, err := repo.db.ExecContext(ctx, sqlStr, args...)

	if err != nil {
		return errs.NewErrorBadQuery("repository update coupon sql exec").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	rowsCount, err := res.RowsAffected()
	if err != nil {
		return errs.NewErrorBadQuery("repository update coupon sql exec result").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	if rowsCount == 0 {
		return errs.NewErrorNotFound("coupon not found")
	}

	return nil
}

func (repo *CouponMariaDb) GetList(ctx context.Context, params dto.GetCoupons) ([]dto.Coupon, error) {
	if params.Limit < 1 || params.Page < 1 {
		return nil, errs.NewErrorBadQuery("repository coupons list params").WithLog(repo.log.Error().Ctx(ctx).Any("params", params))
	}

	builder := squirrel.
		Select("uuid", "title", "description", "promocode").
		From(couponsTable).
		Offset(uint64(params.Limit) * (params.Page - 1)).
		Limit(uint64(params.Limit))

	builder = repo.setListFilter(builder, params)

	sqlStr, args, err := builder.ToSql()

	if err != nil {
		return nil, errs.NewErrorBadQuery("repository coupons list sql").WithLog(repo.log.Error().Ctx(ctx).Err(err).Any("params", params))
	}

	rows, err := repo.db.QueryContext(ctx, sqlStr, args...)

	if err != nil {
		return nil, errs.NewErrorBadQuery("repository coupons list exec result").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	defer rows.Close()

	var list []dto.Coupon

	for rows.Next() {
		var item dto.Coupon
		var uuidStr string
		if err := rows.Scan(&uuidStr, &item.Title, &item.Description, &item.Promocode); err != nil {
			return nil, errs.NewErrorBadQuery("repository coupons list parse result").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
		}
		item.Uuid, err = uuid.Parse(uuidStr)
		if err != nil {
			return nil, errs.NewErrorBadQuery("repository coupons list parse result uuid").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("uuid", uuidStr))
		}

		list = append(list, item)
	}

	return list, nil
}

func (repo *CouponMariaDb) GetListTotal(ctx context.Context, params dto.GetCoupons) (uint64, error) {
	builder := squirrel.
		Select("COUNT(*) AS total").
		From(couponsTable)

	builder = repo.setListFilter(builder, params)

	sqlStr, args, err := builder.ToSql()

	if err != nil {
		return 0, errs.NewErrorBadQuery("repository coupons list count sql").WithLog(repo.log.Error().Ctx(ctx).Err(err).Any("params", params))
	}

	row := repo.db.QueryRowContext(ctx, sqlStr, args...)

	var total uint64

	if err := row.Scan(&total); err != nil {
		return 0, errs.NewErrorBadQuery("repository coupons list count exec").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	return total, nil
}

func (repo *CouponMariaDb) GetByUuid(ctx context.Context, uuidVal uuid.UUID) (dto.Coupon, error) {

	if uuidVal == uuid.Nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("repository get by id empty uuid").WithLog(repo.log.Error().Ctx(ctx))
	}

	sqlStr, args, err := squirrel.
		Select("uuid", "title", "description", "promocode").
		From(couponsTable).
		Where(squirrel.Eq{"uuid": uuidVal.String()}).
		ToSql()

	if err != nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("repository get by id coupon sql").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("uuid", uuidVal.String()))
	}

	row := repo.db.QueryRowContext(ctx, sqlStr, args...)

	var item dto.Coupon
	var uuidStr string
	if err := row.Scan(&uuidStr, &item.Title, &item.Description, &item.Promocode); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return dto.Coupon{}, errs.NewErrorNotFound("coupon not found")
		}
		return dto.Coupon{}, errs.NewErrorBadQuery("repository get by id coupon parse result").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	item.Uuid, err = uuid.Parse(uuidStr)
	if err != nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("repository get by id coupon parse result uuid").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("uuid", uuidStr))
	}

	return item, nil
}

func (repo *CouponMariaDb) GetByPromocode(ctx context.Context, promocode string) (dto.Coupon, error) {
	if promocode == "" {
		return dto.Coupon{}, errs.NewErrorBadQuery("repository get by promocode empty promocode").WithLog(repo.log.Error().Ctx(ctx))
	}

	sqlStr, args, err := squirrel.
		Select("uuid", "title", "description", "promocode").
		From(couponsTable).
		Where(squirrel.Eq{"promocode": promocode}).
		ToSql()

	if err != nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("repository get by promocode coupon sql").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("promocode", promocode))
	}

	row := repo.db.QueryRowContext(ctx, sqlStr, args...)

	var item dto.Coupon
	var uuidStr string
	if err := row.Scan(&uuidStr, &item.Title, &item.Description, &item.Promocode); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return dto.Coupon{}, errs.NewErrorNotFound("coupon not found")
		}
		return dto.Coupon{}, errs.NewErrorBadQuery("repository get by promocode coupon parse result").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	item.Uuid, err = uuid.Parse(uuidStr)
	if err != nil {
		return dto.Coupon{}, errs.NewErrorBadQuery("repository get by promocode coupon parse result uuid").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("uuid", uuidStr))
	}

	return item, nil
}

func (repo *CouponMariaDb) DeleteByUuid(ctx context.Context, uuidVal uuid.UUID) error {
	if uuidVal == uuid.Nil {
		return errs.NewErrorBadQuery("repository delete by id empty uuid").WithLog(repo.log.Error().Ctx(ctx))
	}

	sqlStr, args, err := squirrel.
		Delete(couponsTable).
		Where(squirrel.Eq{"uuid": uuidVal.String()}).
		ToSql()

	if err != nil {
		return errs.NewErrorBadQuery("repository delete coupon sql").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("uuid", uuidVal.String()))
	}

	res, err := repo.db.ExecContext(ctx, sqlStr, args...)
	if err != nil {
		return errs.NewErrorBadQuery("repository delete coupon sql exec").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	rowsCount, err := res.RowsAffected()
	if err != nil {
		return errs.NewErrorBadQuery("repository delete coupon sql exec result").WithLog(repo.log.Error().Ctx(ctx).Err(err).Str("sql", sqlStr).Any("args", args))
	}

	if rowsCount == 0 {
		return errs.NewErrorNotFound("coupon not found")
	}

	return nil
}

func (repo *CouponMariaDb) setListFilter(builder squirrel.SelectBuilder, params dto.GetCoupons) squirrel.SelectBuilder {
	if params.Title != "" {
		builder = builder.Where("title LIKE ?", fmt.Sprint("%", params.Title, "%"))
	}

	return builder
}
