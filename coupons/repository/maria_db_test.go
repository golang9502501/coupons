package repository

import (
	"context"
	"coupons/coupons/dto"
	"coupons/coupons/errs"
	"coupons/internal/helper"
	"coupons/internal/logger"
	"database/sql/driver"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"log"
	"testing"
)

func TestCouponRepo_Create(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	ctx := context.Background()
	repo := NewCouponMariaDb(db, logger.NewLogger(zerolog.Disabled))

	type expectation struct {
		err error
		sql func()
	}

	tests := map[string]struct {
		request  dto.CreateCoupon
		expected expectation
	}{
		"success": {
			request: dto.CreateCoupon{
				Title:       "-30% на крем от загара",
				Description: "Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками",
				Promocode:   "ЛЕТО-2024-SUMMER",
			},
			expected: expectation{
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`INSERT INTO coupons \(uuid,title,description,promocode\) VALUES \(\?,\?,\?,\?\) RETURNING uuid`).
						WithArgs(helper.AnyUuid{}, "-30% на крем от загара", "Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками", "ЛЕТО-2024-SUMMER").
						WillReturnRows(sqlmock.NewRows([]string{"uuid"}).AddRow(uuid.New().String()))
				},
			},
		},
		"failed_empty_result": {
			request: dto.CreateCoupon{
				Title:       "-30% на крем от загара",
				Description: "",
				Promocode:   "ЛЕТО-2024-SUMMER",
			},
			expected: expectation{
				err: errs.NewErrorBadQuery("repository create coupon result scan"),
				sql: func() {
					mock.
						ExpectQuery(`INSERT INTO coupons \(uuid,title,description,promocode\) VALUES \(\?,\?,\?,\?\) RETURNING uuid`).
						WithArgs(helper.AnyUuid{}, "-30% на крем от загара", "", "ЛЕТО-2024-SUMMER").
						WillReturnRows(sqlmock.NewRows([]string{"uuid"}))
				},
			},
		},
		"failed_sql_error": {
			request: dto.CreateCoupon{
				Title:       "-30% на крем от загара",
				Description: "",
				Promocode:   "ЛЕТО-2024-SUMMER",
			},
			expected: expectation{
				err: errs.NewErrorBadQuery("repository create coupon result scan"),
				sql: func() {
					mock.
						ExpectQuery(`INSERT INTO coupons \(uuid,title,description,promocode\) VALUES \(\?,\?,\?,\?\) RETURNING uuid`).
						WithArgs(helper.AnyUuid{}, "-30% на крем от загара", "", "ЛЕТО-2024-SUMMER").
						WillReturnError(errors.New("some error"))
				},
			},
		},
		"failed_invalid_uuid": {
			request: dto.CreateCoupon{
				Title:       "-30% на крем от загара",
				Description: "",
				Promocode:   "ЛЕТО-2024-SUMMER",
			},
			expected: expectation{
				err: errs.NewErrorBadQuery("repository create coupon result scan uuid"),
				sql: func() {
					mock.
						ExpectQuery(`INSERT INTO coupons \(uuid,title,description,promocode\) VALUES \(\?,\?,\?,\?\) RETURNING uuid`).
						WithArgs(helper.AnyUuid{}, "-30% на крем от загара", "", "ЛЕТО-2024-SUMMER").
						WillReturnRows(sqlmock.NewRows([]string{"uuid"}).AddRow("some invalid uuid"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			_, err := repo.Create(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			}
		})
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Err -> \nThere were unfulfilled expectations: %s", err)
	}
}

func TestCouponRepo_Update(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	ctx := context.Background()
	repo := NewCouponMariaDb(db, logger.NewLogger(zerolog.Disabled))

	type expectation struct {
		err error
		sql func()
	}

	tests := map[string]struct {
		request  dto.EditCoupon
		expected expectation
	}{
		"success_full_request": {
			request: dto.EditCoupon{
				Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title:       helper.PStr("-30% на крем от загара"),
				Description: helper.PStr("Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками"),
				Promocode:   helper.PStr("ЛЕТО-2024-SUMMER"),
			},
			expected: expectation{
				err: nil,
				sql: func() {
					mock.
						ExpectExec(`UPDATE coupons SET title = \?, description = \?, promocode = \? WHERE uuid = \?`).
						WithArgs("-30% на крем от загара", "Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками", "ЛЕТО-2024-SUMMER", "1a37bfa8-1cbc-4327-afb7-9d2591a5800e").
						WillReturnResult(sqlmock.NewResult(1, 1))
				},
			},
		},
		"success_partially_request": {
			request: dto.EditCoupon{
				Uuid:  uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title: helper.PStr("-30% на крем от загара"),
			},
			expected: expectation{
				err: nil,
				sql: func() {
					mock.
						ExpectExec(`UPDATE coupons SET title = \? WHERE uuid = \?`).
						WithArgs("-30% на крем от загара", "1a37bfa8-1cbc-4327-afb7-9d2591a5800e").
						WillReturnResult(sqlmock.NewResult(1, 1))
				},
			},
		},
		"failed_empty_request": {
			request: dto.EditCoupon{
				Uuid: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			},
			expected: expectation{
				err: errs.NewErrorBadQuery("repository empty coupon update data"),
				sql: func() {},
			},
		},
		"failed_empty_uuid": {
			request: dto.EditCoupon{
				Title:       helper.PStr("-30% на крем от загара"),
				Description: helper.PStr("Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками"),
				Promocode:   helper.PStr("ЛЕТО-2024-SUMMER"),
			},
			expected: expectation{
				err: errs.NewErrorBadQuery("repository empty coupon update uuid"),
				sql: func() {},
			},
		},
		"failed_sql": {
			request: dto.EditCoupon{
				Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title:       helper.PStr("-30% на крем от загара"),
				Description: helper.PStr("Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками"),
				Promocode:   helper.PStr("ЛЕТО-2024-SUMMER"),
			},
			expected: expectation{
				err: errs.NewErrorBadQuery("repository update coupon sql exec"),
				sql: func() {
					mock.
						ExpectExec(`UPDATE coupons SET title = \?, description = \?, promocode = \? WHERE uuid = \?`).
						WithArgs("-30% на крем от загара", "Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками", "ЛЕТО-2024-SUMMER", "1a37bfa8-1cbc-4327-afb7-9d2591a5800e").
						WillReturnError(errors.New("some error"))
				},
			},
		},
		"failed_no_row_affected": {
			request: dto.EditCoupon{
				Uuid:        uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
				Title:       helper.PStr("-30% на крем от загара"),
				Description: helper.PStr("Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками"),
				Promocode:   helper.PStr("ЛЕТО-2024-SUMMER"),
			},
			expected: expectation{
				err: errs.NewErrorNotFound("coupon not found"),
				sql: func() {
					mock.
						ExpectExec(`UPDATE coupons SET title = \?, description = \?, promocode = \? WHERE uuid = \?`).
						WithArgs("-30% на крем от загара", "Скидка -30% на все товары категории \"Крем от загара\". Промокод не суммируется с другими скидками", "ЛЕТО-2024-SUMMER", "1a37bfa8-1cbc-4327-afb7-9d2591a5800e").
						WillReturnResult(sqlmock.NewResult(1, 0))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			if errT := helper.TestCompErrs(repo.Update(ctx, tt.request), tt.expected.err); errT != nil {
				t.Error(errT.Error())
			}
		})
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Err -> \nThere were unfulfilled expectations: %s", err)
	}
}

func TestCouponRepo_GetList(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	ctx := context.Background()
	repo := NewCouponMariaDb(db, logger.NewLogger(zerolog.Disabled))

	type expectation struct {
		response []dto.Coupon
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  dto.GetCoupons
		expected expectation
	}{
		"success_full_first_page": {
			request: dto.GetCoupons{
				Page:  1,
				Limit: 10,
				Title: "Test",
			},
			expected: expectation{
				response: []dto.Coupon{
					{
						Title:       "Test-купон-1",
						Description: "Test-Описание-1",
						Promocode:   "Test-промокод-1",
					},
					{
						Title:       "Test-купон-2",
						Description: "",
						Promocode:   "Test-промокод-2",
					},
					{
						Title:       "",
						Description: "",
						Promocode:   "",
					},
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE title LIKE \? LIMIT 10 OFFSET 0`).
						WithArgs("%Test%").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRows(
									[]driver.Value{uuid.New().String(), "Test-купон-1", "Test-Описание-1", "Test-промокод-1"},
									[]driver.Value{uuid.New().String(), "Test-купон-2", "", "Test-промокод-2"},
									[]driver.Value{uuid.New().String(), "", "", ""},
								),
						)
				},
			},
		},
		"success_empty_next_page": {
			request: dto.GetCoupons{
				Page:  3,
				Limit: 20,
			},
			expected: expectation{
				response: []dto.Coupon{},
				err:      nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons LIMIT 20 OFFSET 40`).
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}))
				},
			},
		},
		"failed_empty_request": {
			request: dto.GetCoupons{},
			expected: expectation{
				response: []dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository coupons list params"),
				sql:      func() {},
			},
		},
		"failed_sql": {
			request: dto.GetCoupons{
				Page:  1,
				Limit: 10,
			},
			expected: expectation{
				response: []dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository coupons list exec result"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons LIMIT 10 OFFSET 0`).
						WillReturnError(errors.New("some error"))
				},
			},
		},
		"failed_invalid_response": {
			request: dto.GetCoupons{
				Page:  1,
				Limit: 10,
			},
			expected: expectation{
				response: []dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository coupons list parse result uuid"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons LIMIT 10 OFFSET 0`).
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRows(
									[]driver.Value{uuid.New().String(), "Test-купон-1", "Test-Описание-1", "Test-промокод-1"},
									[]driver.Value{uuid.New().String(), "Test-купон-2", "", "Test-промокод-2"},
									[]driver.Value{"invalid uuid", "", "", ""},
								),
						)
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			res, err := repo.GetList(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if len(res) != len(tt.expected.response) {
					t.Errorf("Out length -> \nWant: %d\nGot : %d", len(tt.expected.response), len(res))
				} else {
					for i, coupon := range res {
						expCoupon := tt.expected.response[i]
						if coupon.Uuid == uuid.Nil ||
							expCoupon.Title != coupon.Title ||
							expCoupon.Description != coupon.Description ||
							expCoupon.Promocode != coupon.Promocode {
							t.Errorf("Out -> \nWant: %q\nGot : %q", expCoupon, coupon)
						}
					}
				}
			}
		})
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Err -> \nThere were unfulfilled expectations: %s", err)
	}
}

func TestCouponRepo_GetListTotal(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	ctx := context.Background()
	repo := NewCouponMariaDb(db, logger.NewLogger(zerolog.Disabled))

	type expectation struct {
		response uint64
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  dto.GetCoupons
		expected expectation
	}{
		"success_total": {
			request: dto.GetCoupons{},
			expected: expectation{
				response: 35,
				err:      nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT COUNT\(\*\) AS total FROM coupons`).
						WillReturnRows(sqlmock.NewRows([]string{"total"}).AddRow("35"))
				},
			},
		},
		"success_with_title_total_0": {
			request: dto.GetCoupons{
				Title: "акция",
			},
			expected: expectation{
				response: 0,
				err:      nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT COUNT\(\*\) AS total FROM coupons WHERE title LIKE \?`).
						WithArgs("%акция%").
						WillReturnRows(sqlmock.NewRows([]string{"total"}).AddRow("0"))
				},
			},
		},
		"failed_sql": {
			request: dto.GetCoupons{},
			expected: expectation{
				response: 0,
				err:      errs.NewErrorBadQuery("repository coupons list count exec"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT COUNT\(\*\) AS total FROM coupons`).
						WillReturnError(errors.New("some sql error"))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			res, err := repo.GetListTotal(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if res != tt.expected.response {
					t.Errorf("Out total -> \nWant: %d\nGot : %d", tt.expected.response, res)
				}
			}
		})
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Err -> \nThere were unfulfilled expectations: %s", err)
	}
}

func TestCouponRepo_GetByUuid(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	ctx := context.Background()
	repo := NewCouponMariaDb(db, logger.NewLogger(zerolog.Disabled))

	type expectation struct {
		response dto.Coupon
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  uuid.UUID
		expected expectation
	}{
		"success": {
			request: uuid.MustParse("2e4ead7d-b3e3-4127-aba3-cc52cae8192a"),
			expected: expectation{
				response: dto.Coupon{
					Uuid:        uuid.MustParse("2e4ead7d-b3e3-4127-aba3-cc52cae8192a"),
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRow("2e4ead7d-b3e3-4127-aba3-cc52cae8192a", "-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234"),
						)
				},
			},
		},
		"failed_empty_uuid": {
			request: uuid.UUID{},
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository get by id empty uuid"),
				sql:      func() {},
			},
		},
		"failed_not_found": {
			request: uuid.MustParse("2e4ead7d-b3e3-4127-aba3-cc52cae8192a"),
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorNotFound("coupon not found"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}))
				},
			},
		},
		"failed_sql": {
			request: uuid.MustParse("2e4ead7d-b3e3-4127-aba3-cc52cae8192a"),
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository get by id coupon parse result"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnError(errors.New("some sql error"))
				},
			},
		},
		"failed_invalid_uuid": {
			request: uuid.MustParse("2e4ead7d-b3e3-4127-aba3-cc52cae8192a"),
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository get by id coupon parse result uuid"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE uuid = \?`).
						WithArgs("2e4ead7d-b3e3-4127-aba3-cc52cae8192a").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRow("some invalid uuid", "-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234"),
						)
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			coupon, err := repo.GetByUuid(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if coupon.Uuid == uuid.Nil ||
					tt.expected.response.Title != coupon.Title ||
					tt.expected.response.Description != coupon.Description ||
					tt.expected.response.Promocode != coupon.Promocode {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}
		})
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Err -> \nThere were unfulfilled expectations: %s", err)
	}
}

func TestCouponRepo_GetByPromocode(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	ctx := context.Background()
	repo := NewCouponMariaDb(db, logger.NewLogger(zerolog.Disabled))

	type expectation struct {
		response dto.Coupon
		err      error
		sql      func()
	}

	tests := map[string]struct {
		request  string
		expected expectation
	}{
		"success": {
			request: "RADUGA1234",
			expected: expectation{
				response: dto.Coupon{
					Uuid:        uuid.MustParse("2e4ead7d-b3e3-4127-aba3-cc52cae8192a"),
					Title:       "-25% на первый и повторные онлайн-заказы",
					Description: "Небольшое описание",
					Promocode:   "RADUGA1234",
				},
				err: nil,
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE promocode = \?`).
						WithArgs("RADUGA1234").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRow("2e4ead7d-b3e3-4127-aba3-cc52cae8192a", "-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234"),
						)
				},
			},
		},
		"failed_empty_promocode": {
			request: "",
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository get by promocode empty promocode"),
				sql:      func() {},
			},
		},
		"failed_not_found": {
			request: "RADUGA1234",
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorNotFound("coupon not found"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE promocode = \?`).
						WithArgs("RADUGA1234").
						WillReturnRows(sqlmock.NewRows([]string{"uuid", "title", "description", "promocode"}))
				},
			},
		},
		"failed_sql": {
			request: "RADUGA1234",
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository get by promocode coupon parse result"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE promocode = \?`).
						WithArgs("RADUGA1234").
						WillReturnError(errors.New("some sql error"))
				},
			},
		},
		"failed_invalid_uuid": {
			request: "RADUGA1234",
			expected: expectation{
				response: dto.Coupon{},
				err:      errs.NewErrorBadQuery("repository get by promocode coupon parse result uuid"),
				sql: func() {
					mock.
						ExpectQuery(`SELECT uuid, title, description, promocode FROM coupons WHERE promocode = \?`).
						WithArgs("RADUGA1234").
						WillReturnRows(
							sqlmock.
								NewRows([]string{"uuid", "title", "description", "promocode"}).
								AddRow("some invalid uuid", "-25% на первый и повторные онлайн-заказы", "Небольшое описание", "RADUGA1234"),
						)
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			coupon, err := repo.GetByPromocode(ctx, tt.request)
			if errT := helper.TestCompErrs(err, tt.expected.err); errT != nil {
				t.Error(errT.Error())
			} else if err == nil {
				if coupon.Uuid == uuid.Nil ||
					tt.expected.response.Title != coupon.Title ||
					tt.expected.response.Description != coupon.Description ||
					tt.expected.response.Promocode != coupon.Promocode {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.response, coupon)
				}
			}
		})
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Err -> \nThere were unfulfilled expectations: %s", err)
	}
}

func TestCouponRepo_DeleteByUuid(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("test db mock: %e", err)
	}

	ctx := context.Background()
	repo := NewCouponMariaDb(db, logger.NewLogger(zerolog.Disabled))

	type expectation struct {
		err error
		sql func()
	}

	tests := map[string]struct {
		request  uuid.UUID
		expected expectation
	}{
		"success": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				err: nil,
				sql: func() {
					mock.
						ExpectExec(`DELETE FROM coupons WHERE uuid = \?`).
						WithArgs("1a37bfa8-1cbc-4327-afb7-9d2591a5800e").
						WillReturnResult(sqlmock.NewResult(1, 1))
				},
			},
		},
		"failed_empty_uuid": {
			request: uuid.UUID{},
			expected: expectation{
				err: errs.NewErrorBadQuery("repository delete by id empty uuid"),
				sql: func() {},
			},
		},
		"failed_sql": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				err: errs.NewErrorBadQuery("repository delete coupon sql exec"),
				sql: func() {
					mock.
						ExpectExec(`DELETE FROM coupons WHERE uuid = \?`).
						WithArgs("1a37bfa8-1cbc-4327-afb7-9d2591a5800e").
						WillReturnError(errors.New("some sql error"))
				},
			},
		},
		"failed_no_row_affected": {
			request: uuid.MustParse("1a37bfa8-1cbc-4327-afb7-9d2591a5800e"),
			expected: expectation{
				err: errs.NewErrorNotFound("coupon not found"),
				sql: func() {
					mock.
						ExpectExec(`DELETE FROM coupons WHERE uuid = \?`).
						WithArgs("1a37bfa8-1cbc-4327-afb7-9d2591a5800e").
						WillReturnResult(sqlmock.NewResult(1, 0))
				},
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			tt.expected.sql()
			if errT := helper.TestCompErrs(repo.DeleteByUuid(ctx, tt.request), tt.expected.err); errT != nil {
				t.Error(errT.Error())
			}
		})
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("Err -> \nThere were unfulfilled expectations: %s", err)
	}
}
