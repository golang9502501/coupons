include .env

step ?=

up:
	docker-compose up -d && docker-compose exec app bash

stop:
	docker-compose stop

run:
	go run cmd/main.go

proto-gen:
	protoc --proto_path=coupons/coupons_gen --go_out=coupons/coupons_gen --go_opt=paths=source_relative --go-grpc_out=coupons/coupons_gen --go-grpc_opt=paths=source_relative coupons.proto

test:
	go test ./coupons/... -v

migration:
	migrate create -ext sql -dir migrations -seq $(name)

migrate:
	migrate -path ./migrations -database "mysql://${DB_USERNAME}:${DB_PASSWORD}@tcp(${DB_HOST}:${DB_PORT})/${DB_DATABASE}" up $(step)

migration-version:
	migrate -path ./migrations -database "mysql://${DB_USERNAME}:${DB_PASSWORD}@tcp(${DB_HOST}:${DB_PORT})/${DB_DATABASE}" force $(v)

migrate-down:
	migrate -path ./migrations -database "mysql://${DB_USERNAME}:${DB_PASSWORD}@tcp(${DB_HOST}:${DB_PORT})/${DB_DATABASE}" down $(step)