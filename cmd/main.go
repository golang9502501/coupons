package main

import (
	"coupons/coupons/config"
	"coupons/coupons/controller"
	"coupons/coupons/coupons_gen"
	"coupons/coupons/repository"
	"coupons/coupons/usecase"
	"coupons/internal/logger"
	"coupons/internal/middleware"
	"coupons/internal/validator"
	"database/sql"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func main() {
	fmt.Println("Welcome to Coupons API!")
	fmt.Println("Initialization...")

	logSrv := logger.NewLogger(zerolog.DebugLevel)

	cnf, err := config.NewConfig()
	if err != nil {
		logSrv.Fatal().Err(err).Msg("config init")
	}

	validate, err := validator.NewValidator()
	if err != nil {
		logSrv.Fatal().Err(err).Msg("validator init")
	}

	db, err := connectDb(cnf, logSrv)
	if err != nil {
		logSrv.Fatal().Err(err).Msg("db init")
	}

	defer func(db *sql.DB) {
		if err := db.Close(); err != nil {
			logSrv.Fatal().Err(err).Msg("db close")
		}
	}(db)

	s := newGrpcServer(cnf)

	coupons_gen.RegisterGrpcCouponsServiceServer(s, getCouponController(db, validate, logSrv))
	reflection.Register(s)

	listener, err := net.Listen("tcp", ":"+strconv.Itoa(cnf.Grpc.Port))
	if err != nil {
		logSrv.Fatal().Err(err).Msg("listener init")
	}

	go func() {
		if err = s.Serve(listener); err != nil {
			logSrv.Fatal().Err(err).Msg("grpc server run")
		}
	}()

	fmt.Println("                       ___          ")
	fmt.Println("                     /API__\\<=     ")
	fmt.Println("                     |.\\/. |       ")
	fmt.Println("Ready to FIGHT!      \\ ~   /       ")
	fmt.Println("                      |  |          ")
	fmt.Println("           DDDo ___._/oDDD\\_.___   ")
	fmt.Println("            \\ \\ /  /     \\ \\/  /")
	fmt.Println("             \\ \\  /   *   \\ \\ / ")
	fmt.Println("                  |          |      ")

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	fmt.Println()
	fmt.Println("Coupons API shutting down")
	fmt.Println("Goodbye!")
}

func connectDb(cnf *config.Config, logSrv *zerolog.Logger) (*sql.DB, error) {
	connect := mysql.NewConfig()
	connect.User = cnf.Database.Username
	connect.Passwd = cnf.Database.Password
	connect.Net = "tcp"
	connect.Addr = cnf.Database.Host + ":" + strconv.Itoa(cnf.Database.Port)
	connect.DBName = cnf.Database.Database
	connect.Logger = logSrv

	return sql.Open("mysql", connect.FormatDSN())
}

func newGrpcServer(cnf *config.Config) *grpc.Server {
	return grpc.NewServer(grpc.UnaryInterceptor(middleware.ContextTimeoutUnaryInterceptor(cnf.Grpc.Timeout)))
}

func getCouponController(db *sql.DB, validate *validator.Validator, logSrv *zerolog.Logger) *controller.CouponController {
	repo := repository.NewCouponMariaDb(db, logSrv)
	uc := usecase.NewCouponUseCase(repo)
	c := controller.NewCouponController(uc, validate)
	return &c
}
