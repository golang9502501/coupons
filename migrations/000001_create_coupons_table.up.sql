CREATE TABLE coupons
(
    uuid        uuid primary key,
    title       varchar(255) not null,
    description text         not null default '',
    promocode   varchar(255) not null unique
);