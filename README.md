# Welcome to Coupon API!

This project was created to study Golang

![Coupons](https://aspro.ru/upload/iblock/a9c/aspro_blog_image.jpg)

### Installation
1. Go to the root directory /coupons
2. Run the command `cp .env.example .env`
3. Run the command `make up`
4. Run the command `make proto-gen`
5. Run the command `make migrate`
6. Run the command `make run`

### Tooltips in the console

- Run containers `make up`
- Run application `make run`
- Stop containers `make stop`
- Generate proto package file `make proto-gen`
- Run tests `make test`
- Generate migration file `make migration name={migration name}`
- Migrate file `make migrate step=1` ("step" is an optional)
- Install latest migration version `make migration-version v={version}`
- Rollback migration `make migrate-down step=1` ("step" is an optional)
- Generate mock `mockgen -source {path/to/interface} -destination {path/to/generate/mock} -package {package name}`